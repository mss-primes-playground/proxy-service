# proxy-service

REST endpoint for mss-primes-playground/prime-number-server>

## Compile / Test / Run

To compile and run this service you need [sbt](https://www.scala-sbt.org/).
:information_source: project has been created with sbt version 1.5.5

```shell
sbt clean test
```

Environment variables used to configure the service

| Name                     | Default value | Description                                                         |
|:-------------------------|:--------------|:--------------------------------------------------------------------|
| PROXY\_SERVER\_INTERFACE | 127.0.0.1     | Interface for endpoint binding                                      |
| PROXY\_SERVER\_PORT      | 8080          | Endpoint binding port                                               |
| PRIMES\_SERVICE\_HOST    | 127.0.0.1     | Host/IP where mss-primes-playground/prime-number-server> is running |
| PRIMES\_SERVICE\_PORT    | 9090          | Binding port used to run mss-primes-playground/proxy-service>       |

Use `sbt run` to start the service with default configuration and `<Ctrl>-C` to stop running one.

If you want to run service with custom settings you can use `-D<ENV_VAR>=<VALUE>` systax.
```shell
sbt run -DPROXY_SERVER_INTERFACE=0.0.0.0 -DPROXY_SERVER_PORT=8080 -DPRIME_SERVICE_HOST=192.168.0.1 -DPRIME_SERVICE_PORT=8080
```
> :warning: This is just a proxy. To get expected string with primes it requires running mss-primes-playground/proxy-service> instance.

You can 'play' with this service using [Postman](https://www.postman.com/) or simple [curl](https://curl.se/)

```shell
curl http://127.0.0.1/primes/1111
```

## Docker

To create Docker image run
```shell
sbt docker:publishLocal
```

If you have `docker-compose` installed

Start both services with port `8080` exposed for `proxy-service`:
> :information_source: you need to run `sbt docker:publishLocal` for mss-primes-playground/prime-number-server> project befor issuing this command
```shell
docker-compose up -d
```

Stop services:
```shell
docker-compose down
```
## REST

### Get Primes

Streams all prime numbers up to a given number, where the output numbers are separated by commas.

**URL** : `/primes/{number}`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Example**

`GET /primes/17` returns `2,3,5,7,11,13,17`
