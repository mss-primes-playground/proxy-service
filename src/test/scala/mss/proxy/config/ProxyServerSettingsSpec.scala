package mss.proxy.config

import com.typesafe.config.ConfigFactory
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class ProxyServerSettingsSpec extends AnyFreeSpec with Matchers {

  val config = ConfigFactory.parseString("""
    |proxy-server {
    |  interface = 0.0.0.0
    |  port = 9090
    |
    |  primes-service {
    |    host = prime-service
    |    port = 8080
    |  }
    |}""".stripMargin).withFallback(ConfigFactory.defaultApplication())

  val settings = new ProxyServerSettings(config)

  "ProxyServerSettings should read correctly from Config" - {
    "interface" in {
      settings.interface shouldBe "0.0.0.0"
    }

    "port" in {
      settings.port shouldBe 9090
    }

    "primes service settings" - {
      val primesServiceSettings = settings.primesServiceSettings

      "host" in {
        primesServiceSettings.host shouldBe "prime-service"
      }

      "port" in {
        primesServiceSettings.port shouldBe 8080
      }

    }
  }

}
