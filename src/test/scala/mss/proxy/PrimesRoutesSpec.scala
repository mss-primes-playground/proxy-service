package mss.proxy

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.{ActorSystem => ClassicActorSystem}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.scaladsl.Source

import mss.primes.proto.{PrimeNumbersService, PrimeReply, PrimesRequest}
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatestplus.mockito.MockitoSugar

class PrimesRoutesSpec extends AnyWordSpec with Matchers with ScalatestRouteTest
    with BeforeAndAfterAll
    with MockitoSugar {

  lazy val typedSystem = ActorSystem[Nothing](Behaviors.empty, actorSystemNameFrom(getClass))

  override protected def createActorSystem(): ClassicActorSystem = typedSystem.classicSystem

  val expectedNumbers = Map[Long, List[Long]] (
    7L -> List[Long](2, 3, 5, 7),
    2L -> List[Long](2)
  )

  val primesService: PrimeNumbersService = {
    val ps = mock[PrimeNumbersService]

    when(ps.primesUpTo(any[PrimesRequest])).thenAnswer { args =>
      expectedNumbers.get(args.getArgument[PrimesRequest](0).upTo)
        .map{ numbers =>
          Source(numbers).map(PrimeReply(_))
        }
      .getOrElse(Source.empty)
    }
    ps
  }

  val routes = new PrimesRoutes(primesService)(typedSystem).primesRoutes

  "Prime route" should {
    for(num <- List[Long](7,2,1))
      s"return correct numbers for $num" in  {
        val expected = expectedNumbers.get(num).map(_.mkString(",")).getOrElse("")
        Get(s"/prime/${num}") ~> routes ~> check {
        responseAs[String] shouldEqual expected
      }
    }
  }

}
