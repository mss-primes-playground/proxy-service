package mss.proxy

import akka.NotUsed
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.common.EntityStreamingSupport
import akka.http.scaladsl.marshalling.{Marshaller, Marshalling}
import akka.http.scaladsl.model.ContentTypes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.scaladsl.{Flow, Source}
import akka.util.ByteString

import mss.primes.proto.{PrimeNumbersService, PrimeReply, PrimesRequest}


class PrimesRoutes(primesService: PrimeNumbersService)(implicit system: ActorSystem[_]) {

  val Comma = ByteString(",")

  implicit val primesMarshallner = Marshaller.strict[PrimeReply, ByteString] { prime =>
    Marshalling.WithFixedContentType(ContentTypes.`text/csv(UTF-8)`, () => {
      val bsNumber = ByteString(prime.number.toString())

      /* since we are generating all primes starting from the beginning
       *  1st one will be always 2.
       *  So, it is simply easier not to add ',' in front of 2 to produce
       *  string like "2, 3, 5, 7"
       */
      if(prime.number == 2L) bsNumber else Comma ++ bsNumber
    })
  }

  implicit val streaming = EntityStreamingSupport.csv()
    .withParallelMarshalling(parallelism = 8, unordered = false)
    .withFramingRenderer(Flow[ByteString].map(identity))

  val primesRoutes: Route = {
    get {
      pathPrefix("prime" / LongNumber) { upTo =>
        val primes = primesService.primesUpTo(PrimesRequest(upTo))

        complete(primes)
      }
    }
  }
}
