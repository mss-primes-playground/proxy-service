package mss.proxy

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.server.Route

object ProxyServer {

  def start(routes: Route)(implicit system: ActorSystem[_]): Unit = {

    implicit val ec: ExecutionContextExecutor = system.executionContext

    val settings = config.ProxyServerSettings(system)

    val bound =
      Http().newServerAt(settings.interface, settings.port)
      .bind(routes)
      .map(_.addToCoordinatedShutdown(3.seconds))

    bound.onComplete{
      case Success(binding) =>
        val address = binding.localAddress
        system.log.info("Proxy server {}:{}", address.getHostString, address.getPort)
      case Failure(e) =>
        system.log.error("Failed to bind http endpoint. Terminating")
        system.terminate()
    }
  }

}
