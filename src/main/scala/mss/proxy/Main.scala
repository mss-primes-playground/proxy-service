package mss.proxy

import scala.util.control.NonFatal

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.grpc.GrpcClientSettings
import akka.http.scaladsl.server.Route

import mss.primes.proto.{PrimeNumbersService, PrimeNumbersServiceClient}
import mss.proxy.config.{PrimesServiceSettings, ProxyServerSettings}
import org.slf4j.LoggerFactory

object Main {

  val log = LoggerFactory.getLogger(this.getClass())

  def main(args: Array[String]): Unit = {
    val system = ActorSystem[Nothing](Behaviors.empty, "ProxyServer")
    try {
      init(system)
    } catch {
      case NonFatal(e) =>
        log.error("Initialization failed. Terminating.", e)
        system.terminate()
    }
  }

  def init(system: ActorSystem[_]): Unit = {
    val primesService = primesServiceClient(system)
    val routes = new PrimesRoutes(primesService)(system)
    ProxyServer.start(routes.primesRoutes)(system)
  }

  private def primesServiceClient(system: ActorSystem[_]): PrimeNumbersService = {

    val primesServiceSettings  = ProxyServerSettings(system).primesServiceSettings

    val primesServiceGprcClientSettings = GrpcClientSettings.connectToServiceAt(
      primesServiceSettings.host,
      primesServiceSettings.port
    )(system).withTls(false)

    PrimeNumbersServiceClient(primesServiceGprcClientSettings)(system)
  }

}
