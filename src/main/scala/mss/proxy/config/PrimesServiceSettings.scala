package mss.proxy.config

import com.typesafe.config.Config

class PrimesServiceSettings(parentConfig: Config) {

  private val primesConfig = parentConfig.getConfig("primes-service")

  /** Host of the PrimesService */
  val host = primesConfig.getString("host")

  /** Port of the PremsService */
  val port = primesConfig.getInt("port")

}
