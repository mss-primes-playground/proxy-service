package mss.proxy.config

import akka.actor.typed.{ActorSystem, Extension, ExtensionId}

import com.typesafe.config.Config

object ProxyServerSettings extends ExtensionId[ProxyServerSettings] {

  val ConfigKey = "proxy-server"

  def createExtension(system: ActorSystem[_]): ProxyServerSettings = {
    new ProxyServerSettings(system.settings.config)
  }

  def get(system: ActorSystem[_]): ProxyServerSettings = apply(system)
}

class ProxyServerSettings(rootConfig: Config) extends Extension {

  val config = rootConfig.getConfig(ProxyServerSettings.ConfigKey)

  /** HTTP server inteface */
  val interface: String = config.getString("interface")

  /** HTTP server port */
  val port: Int = config.getInt("port")

  val primesServiceSettings = new PrimesServiceSettings(config)

}
